<?php

$root = "/worklog";

require 'Slim/Slim.php';

$app = new Slim();

$app->get('/worklogs', 'getWorklogs');
$app->get('/worklogs/:id', 'getWorklog');
$app->get('/projects', 'getProjects');
$app->get('/projects/:project',	'getProject');
$app->get('/worklogs/search/:query', 'findByProject');
$app->get('/auth/logged_in', 'auth');
$app->post('/worklogs', 'addWorklog');
$app->put('/worklogs/:id', 'updateWorklog');
$app->delete('/worklogs/:id', 'deleteWorklog');

$app->run();

function auth() {
	echo '{"success":"true"}';
}

function getWorklogs() {
	$sql = "select * FROM worklog ORDER BY time_start";
	try {
		$db = getConnection();
		$stmt = $db->query($sql);
		$wklogs = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		// echo '{"worklog": ' . json_encode($worklogs) . '}';
		echo json_encode($wklogs);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function getWorklog($id) {
	$sql = "SELECT * FROM worklog WHERE id=:id";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$wklog = $stmt->fetchObject();
		$db = null;
		echo json_encode($wklog);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function getProjects() {
	$sql = "SELECT distinct project FROM worklog";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$projects = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($projects);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function getProject($project) {
	$sql = "SELECT time_start,time_end FROM worklog WHERE project=:project";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("project", $project);
		$stmt->execute();
		$worklogs = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		$proj = new StdClass();
		$proj->project = $project;
		$proj->total_hours = 0;
		foreach ($worklogs as $wklog) {
			$proj->total_hours += (strtotime($wklog->time_end) - strtotime($wklog->time_start)) / 3600;
		}
		echo json_encode($proj);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function addWorklog() {
	error_log('addWorklog\n', 3, '/var/tmp/php.log');
	$request = Slim::getInstance()->request();
	$worklog = json_decode($request->getBody());
	$sql = "INSERT INTO worklog (project, time_start, time_end, description) VALUES (:project, NOW(), NULL, :description)";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("project", $worklog->project);
		// $stmt->bindParam("time_start", $worklog->time_start);
		// $stmt->bindParam("time_end", $worklog->time_end);
		$stmt->bindParam("description", $worklog->description);
		$stmt->execute();
		$worklog->id = $db->lastInsertId();
		$db = null;
		echo json_encode($worklog);
	} catch(PDOException $e) {
		error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function updateWorklog($id) {
	$request = Slim::getInstance()->request();
	$body = $request->getBody();
	$worklog = json_decode($body);
	$time_start = date('Y-m-d H:i:s', strtotime($worklog->time_start));
	$time_end = date('Y-m-d H:i:s', strtotime($worklog->time_end));
	$sql = "UPDATE worklog SET project=:project, time_start=:time_start, time_end=:time_end, description=:description WHERE id=:id";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("project", $worklog->project);
		$stmt->bindParam("time_start", $time_start);
		$stmt->bindParam("time_end", $time_end);
		$stmt->bindParam("description", $worklog->description);
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$db = null;
		echo json_encode($worklog);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function deleteWorklog($id) {
	$sql = "DELETE FROM worklog WHERE id=:id";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$db = null;
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function findByProject($query) {
	$sql = "SELECT * FROM worklog WHERE UPPER(project) LIKE :query ORDER BY project";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$query = "%".$query."%";
		$stmt->bindParam("query", $query);
		$stmt->execute();
		$worklogs = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($worklogs);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}

function getConnection() {
	$dbhost="127.0.0.1";
	$dbuser="";
	$dbpass="";
	$dbname="";
	$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $dbh;
}

?>
