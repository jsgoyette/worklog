window.WorklogListView = Backbone.View.extend({

    tagName:'ul',

    initialize:function () {
        this.model.bind("reset", this.render, this);
        var self = this;
        this.model.bind("add", function (worklog) {
            $(self.el).prepend(new WorklogListItemView({model:worklog}).render().el);
        });
    },

    render:function (eventName) {
        _.each(this.model.models, function (worklog) {
            $(this.el).prepend(new WorklogListItemView({model:worklog}).render().el);
        }, this);
        return this;
    }
});

window.WorklogListItemView = Backbone.View.extend({

    tagName:"li",

    initialize:function () {
        this.template = _.template(tpl.get('worklog-list-item'));
        this.model.bind("change", this.render, this);
        this.model.bind("destroy", this.close, this);
    },

    render:function (eventName) {
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    }

});