window.WorklogView = Backbone.View.extend({

    tagName:"div", // Not required since 'div' is the default if no el or tagName specified

    initialize:function () {

        this.template = _.template(tpl.get('worklog-details'));
        this.model.bind("change", this.render, this);
    },

    render:function (eventName) {
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    },

    events:{
        "change input":"change",
        "click .save":"saveWorklog",
        "click .closelog":"closeWorklog",
        "click .delete":"deleteWorklog",
        "click .project":"viewProject",
        "click .project-summary":"viewProjectSummary"
    },

    change:function (event) {
        var target = event.target;
        // console.log('changing ' + target.id + ' from: ' + target.defaultValue + ' to: ' + target.value);
        // You could change your model on the spot, like this:
        // var change = {};
        // change[target.name] = target.value;
        // this.model.set(change);
    },

    saveWorklog:function () {
        var start = $('#time_start').val();
        start = start != '' ? start : getDate();
        this.model.set({
            project:$('#project').val().toLowerCase(),
            time_start:start,
            time_end:$('#time_end').val(),
            description:$('#description').val(),
        });
        if (this.model.isNew()) {
            var self = this;
            app.worklogList.create(this.model, {
                success:function () {
                    app.navigate('worklogs/' + self.model.id, false);
                    // app.navigate('', true);
                }
            });
        } else {
            this.model.save();
            // app.navigate('', true);
        }
        return false;
    },

    closeWorklog:function () {
        this.model.set({
            project:$('#project').val().toLowerCase(),
            time_start:$('#time_start').val(),
            time_end:getDate(),
            description:$('#description').val(),
        });
        this.model.save();
        return false;
    },

    deleteWorklog:function () {
        if (confirm('Do you really want to delete this entry?')) {
            this.model.destroy({
                success:function () {
                    alert('Worklog deleted successfully');
                    // window.history.back();
                }
            });
            app.navigate('', true);
        }
        return false;
    },

    viewProject:function () {
        app.navigate('/projects/'+this.model.attributes.project, true);
        return false;
    },

    viewProjectSummary:function () {
        app.navigate('/projects/summary/'+this.model.attributes.project, true);
        return false;
    }

});

function getDate () {
    var date = new Date();
    return date.getFullYear() + '-' + ('00' + (date.getMonth()+1)).slice(-2) + '-' + ('00' + (date.getDate())).slice(-2)  + ' ' + ('00' + date.getHours()).slice(-2) + ':' + ('00' + date.getMinutes()).slice(-2) + ':' + ('00' + date.getSeconds()).slice(-2);
}