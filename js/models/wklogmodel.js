window.Worklog = Backbone.Model.extend({
    urlRoot:"api/worklogs",
    defaults:{
        "id":null,
        "project":"",
        "time_start":"",
        "time_end":"",
        "description":"",
    }
});

window.WorklogCollection = Backbone.Collection.extend({
    model:Worklog,
    url:"api/worklogs"
});