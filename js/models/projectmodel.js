window.Project = Backbone.Model.extend({
    urlRoot:"api/projects",
    defaults:{
        "project":"",
        "total_hours":""
    }
});

window.ProjectCollection = Backbone.Collection.extend({
    model:Project,
    url:"api/projects"
});