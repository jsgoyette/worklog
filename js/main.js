Backbone.View.prototype.close = function () {
	// console.log('Closing view ' + this);
	if (this.beforeClose) {
		this.beforeClose();
	}
	this.remove();
	this.unbind();
};

var AppRouter = Backbone.Router.extend({

	initialize: function () {
		$('#header').html(new HeaderView().render().el);
	},

	routes: {
		"":"list",
		"worklogs/new":"newWorklog",
		"worklogs/:id":"worklogDetails",
		"projects/:project":"projectHours",
		"projects/summary/:project":"projectSummary"
	},

	list: function () {
		// this.currentView.close();
		this.before(function () {
			$('#content').html('<h2>Welcome to Backbone Worklog</h2><p>This is a sample application part of of three-part tutorial showing how to build a CRUD application with Backbone.js.</p>');
		});
	},

	worklogDetails: function (id) {
		this.before(function () {
			var worklog = app.worklogList.get(id);
			// console.log(worklog);
			app.showView('#content', new WorklogView({model:worklog}));
		});
	},

	projectHours: function (project) {
		this.before(function () {
			$('#content').html('<h2>'+project+'</h2>');
			var projGroup = _.filter(app.worklogList.models, function (model) {
				return model.attributes.project.toLowerCase() == project;
			});
			var byMonth = _.groupBy(projGroup, function (log) {
				var re = /[-:]| /,
					start = log.attributes.time_start,
					date = start.split(re).map(Number);
				return date[1]+'/'+date[0];
			});

			_.each(byMonth, function (logs, month) {
				var sum = _.reduce(logs, function (memo, log) {
					var diff = hours_diff(log.attributes.time_start, log.attributes.time_end);
					return memo + diff;
				}, 0);
				$('#content').append('<h2>'+month+'</h2><p>Total hours: '+sum.toFixed(2)+' hours</p>');
			})
		});
	},

	projectSummary: function (project) {
		this.before(function () {
			$('#content').html('<h2>'+project+'</h2>');
			var projGroup = _.filter(app.worklogList.models, function (model) {
				return model.attributes.project.toLowerCase() == project;
			});
			console.log(projGroup);
			var byMonth = _.groupBy(projGroup, function (log) {
				var re = /[-:]| /,
					start = log.attributes.time_start,
					date = start.split(re).map(Number);
				return date[1]+'/'+date[0];
			});

			_.each(byMonth, function (logs, month) {
				$('#content').append('<h2>'+month+'</h2>');
				_.each(logs, function (log) {
					log = log.attributes;
					var diff = hours_diff(log.time_start, log.time_end);
					$('#content').append('<h4>'+log.time_start+' - '+log.time_end+'</h4>');
					$('#content').append('<p>'+diff.toFixed(2)+' hours</p><p>'+log.description+'</p>');
				});
			})
		});
	},

	newWorklog: function () {
		this.before(function () {
			app.showView('#content', new WorklogView({model:new Worklog()}));
			$('#project').focus();
		});
	},

	showView: function (selector, view) {
		if (view != this.currentView) {
			if (this.currentView)
				this.currentView.close();
			$(selector).html(view.render().el);
			this.currentView = view;
		}
		return view;
	},

	before: function (callback) {
		if (this.worklogList) {
			if (callback) callback();
		} else {
			// render sidebar
			this.worklogList = new WorklogCollection();
			this.worklogList.fetch({success:function () {
				$('#sidebar').html(new WorklogListView({model:app.worklogList}).render().el);
				if (callback) callback();
			}});
		}
	}

});

function hours_diff (start, end) {
	var re = /[-:]| /,
		date1 = end.split(re).map(Number),
		date2 = start.split(re).map(Number);
	var diffMs = Date.UTC( date1[0], date1[1] - 1, date1[2], date1[3], date1[4], date1[5] ) -
				 Date.UTC( date2[0], date2[1] - 1, date2[2], date2[3], date2[4], date2[5] );
	var hours = diffMs / (3600 * 1000);
	return isNaN(hours) ? 0 : hours;
}

tpl.loadTemplates(['header', 'project-details', 'worklog-details', 'worklog-list-item'], function () {
	app = new AppRouter();
	Backbone.history.start();
});